import { ApiProperty } from '@nestjs/swagger';
import { ObjectLiteral } from 'typeorm';

export class IUpdateResult {
  @ApiProperty()
  raw: any;
  /**
   * Number of affected rows/documents
   * Not all drivers support this
   */
  @ApiProperty()
  affected?: number;
  /**
   * Contains inserted entity id.
   * Has entity-like structure (not just column database name and values).
   */
  /**
   * Generated values returned by a database.
   * Has entity-like structure (not just column database name and values).
   */
  @ApiProperty()
  generatedMaps: ObjectLiteral[];
}

export class IDeleteResult {
  @ApiProperty()
  raw: any;

  @ApiProperty()
  affected?: number;
}
