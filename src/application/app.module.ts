import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentsModule } from '../students/students.module';
import { TypeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { GroupsModule } from '../groups/groups.module';
import { CoursesModule } from '../courses/courses.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from '../marks/marks.module';
import { ConfigModule } from '../configs/config.module';
import { UsersModule } from 'src/users/users.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(TypeOrmAsyncConfig),
    ConfigModule,
    StudentsModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    MarksModule,
    UsersModule,
    AuthModule,
  ],
  providers: [AppService],
})
export class AppModule {}
//
