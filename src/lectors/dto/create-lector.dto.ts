import { ApiProperty } from '@nestjs/swagger';
import { Length, IsNotEmpty, IsString } from 'class-validator';
import { CourseResponse } from 'src/courses/dto/create-course.dto';
import { Course } from 'src/courses/entities/course.entity';
import { Student } from 'src/students/entities/student.entity';

export class CreateLectorDto {
  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  email: string;

  @Length(8, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  password: string;
}

export class LectorResponse extends CreateLectorDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
}

export class LectorResponseWithCourses extends LectorResponse {
  @ApiProperty({ type: () => CourseResponse, isArray: true })
  courses: CourseResponse;
}

export class CoursesLectorExample {
  @ApiProperty()
  courseId: number;
  @ApiProperty()
  courseName: string;
}
export class LectorCourses {
  @ApiProperty()
  id: number;
  @ApiProperty({ type: () => CoursesLectorExample, isArray: true })
  courses: CoursesLectorExample;
}
