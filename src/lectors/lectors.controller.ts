import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpStatus,
} from '@nestjs/common';
import { LectorsService } from './lectors.service';
import {
  CreateLectorDto,
  LectorCourses,
  LectorResponse,
  LectorResponseWithCourses,
} from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';

@UseGuards(AuthGuard)
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @Post()
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Create  Lector' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created Lector',
    type: LectorResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async create(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsService.create(createLectorDto);
  }

  @Get()
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Get Lectors' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get Lectors',
    type: [LectorResponse],
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async findAll() {
    return this.lectorsService.findAll();
  }

  @Get(':id')
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Get Lector' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get Lector',
    type: LectorResponseWithCourses,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector with that id not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async findOne(@Param('id') id: string) {
    return this.lectorsService.findOne(+id);
  }

  @Patch('/:lectorId/courses/:courseId')
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Add course to lector' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Add course to lector',
    type: LectorResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector or course with that id not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async addLectorToCourse(
    @Param('lectorId') lectorId: number,
    @Param('courseId') courseId: number,
  ) {
    return this.lectorsService.addLectorToCourse(lectorId, courseId);
  }

  @Get(':id/courses')
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Get Lector courses' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get Lector courses',
    type: LectorCourses,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector with that id not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async getLectorCourses(@Param('id') id: string) {
    return this.lectorsService.getLectorCourses(+id);
  }
}
