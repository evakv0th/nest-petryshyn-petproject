import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from 'src/courses/entities/course.entity';
import { DataSource, Repository, getConnection } from 'typeorm';
import { Lector } from './entities/lector.entity';
import { AppDataSource } from 'src/configs/database/data-source';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector) private lectorsRepository: Repository<Lector>,
    @InjectRepository(Course) readonly coursesRepository: Repository<Course>,
  ) {}

  async create(createLectorDto: CreateLectorDto) {
    const lector = await this.lectorsRepository.findOne({
      where: [{ name: createLectorDto.name }, { email: createLectorDto.email }],
    });
    if (lector) {
      throw new HttpException(
        'Lector with this name or email already exists',
        HttpStatus.NOT_FOUND,
      );
    }
    return this.lectorsRepository.save(createLectorDto);
  }

  async findAll() {
    const lectors = await this.lectorsRepository.find({});

    return lectors;
  }

  async findOne(id: number) {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .select([
        'lector.id as id',
        'lector.name as name',
        'lector.email as email',
        'lector.password as password',
      ])
      .where('lector.id = :id', { id })
      .getRawOne();

    if (!lector) {
      throw new HttpException(
        'Lector with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }

    lector.courses = await this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.lectors', 'lectors')
      .select([
        'course.id as "courseId"',
        'course.name as "courseName"',
        'course.description as "courseDescription"',
        'course.hours as "courseHours"',
        'course.created_at as "createdAt"',
        'course.updated_at as "updatedAt"',
      ])
      .where('lectors.id = :id', { id })
      .getRawMany();

    return lector;
  }
  //
  async addLectorToCourse(lectorId: number, courseId: number) {
    const lector = await this.lectorsRepository.findOne({
      where: {
        id: lectorId,
      },
    });
    if (!lector) {
      throw new HttpException(
        'Lector with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    const course = await this.coursesRepository.findOne({
      where: {
        id: courseId,
      },
    });

    if (!course) {
      throw new HttpException(
        'Course with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    await this.lectorsRepository
      .createQueryBuilder()
      .relation(Lector, 'courses')
      .of(lector)
      .add(course);

    return lector;
  }

  async getLectorCourses(id: number) {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .select(['lector.id as id'])
      .where('lector.id = :id', { id })
      .getRawOne();

    if (!lector) {
      throw new HttpException(
        'Lector with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }

    lector.courses = await this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.lectors', 'lectors')
      .select(['course.id as "courseId"', 'course.name as "courseName"'])
      .where('lectors.id = :id', { id })
      .getRawMany();

    return lector;
  }
}
