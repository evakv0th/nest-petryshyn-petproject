import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { LectorsController } from './lectors.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from 'src/courses/entities/course.entity';
import { Lector } from './entities/lector.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Course, Lector])],
  controllers: [LectorsController],
  providers: [LectorsService],
})
export class LectorsModule {}
