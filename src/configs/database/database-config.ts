import dotenv from 'dotenv';
import { extname } from 'path';
import { DataSourceOptions, getMetadataArgsStorage } from 'typeorm';

dotenv.config();

export const databaseConfiguration = (): DataSourceOptions => {
  const env = process.env.NODE_ENV;
  const migrationFolder =
    env === 'production'
      ? `dist/database/migrations`
      : 'src/database/migrations';
  const ext = extname(__filename);

  return {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [
      __dirname + `/../../*/entities/*.entity${ext}`,
      ...getMetadataArgsStorage().tables.map((tbl) => tbl.target),
    ],
    migrations: [`${migrationFolder}/*${ext}`],
    migrationsRun: true,
    synchronize: false,
  };
}; //
