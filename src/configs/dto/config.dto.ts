import { IsNumberString, IsString, IsNotEmpty, IsEnum } from 'class-validator';
import { NodeEnvs } from '../../application/enums/app.envs';

export class ConfigDto {
  @IsNotEmpty()
  @IsString()
  @IsEnum(NodeEnvs)
  NODE_ENV: NodeEnvs;

  @IsNotEmpty()
  @IsNumberString()
  SERVER_PORT: number;

  @IsNotEmpty()
  @IsNumberString()
  DATABASE_PORT: number;

  @IsNotEmpty()
  @IsString()
  DATABASE_HOST: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_USERNAME: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_PASSWORD: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_NAME: string;
}
