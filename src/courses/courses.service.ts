import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Course } from './entities/course.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course) private coursesRepository: Repository<Course>,
  ) {}
  async create(createCourseDto: CreateCourseDto) {
    const course = await this.coursesRepository.findOne({
      where: [
        { name: createCourseDto.name },
        { description: createCourseDto.description },
      ],
    });
    if (course) {
      throw new HttpException(
        'Course with this name or description already exists',
        HttpStatus.NOT_FOUND,
      );
    }
    return this.coursesRepository.save(createCourseDto);
  }

  async findAll() {
    const courses = await this.coursesRepository.find({});
    return courses;
  }

  async findOne(id: number) {
    const course = await this.coursesRepository
      .createQueryBuilder('course')
      .where('course.id = :id', { id })
      .getOne();
    if (!course) {
      throw new HttpException(
        'Course with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return course;
  }
}
