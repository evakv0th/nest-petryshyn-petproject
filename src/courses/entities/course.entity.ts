import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'integer',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses)
  @JoinTable({
    name: 'lectorCourses',
  })
  lectors: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
