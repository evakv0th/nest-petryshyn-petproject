import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsString,
  Length,
  Max,
  Min,
} from 'class-validator';

export class CreateCourseDto {
  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @Length(10, 200)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  description: string;

  @Min(50)
  @Max(420)
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  hours: number;
}

export class CourseResponse extends CreateCourseDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
}
