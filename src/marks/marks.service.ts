import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateMarkDto } from './dto/create-mark.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from 'src/courses/entities/course.entity';
import { Lector } from 'src/lectors/entities/lector.entity';
import { Student } from 'src/students/entities/student.entity';
import { Repository } from 'typeorm';
import { Mark } from './entities/mark.entity';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Student) private studentsRepository: Repository<Student>,
    @InjectRepository(Lector) readonly lectorsRepository: Repository<Lector>,
    @InjectRepository(Course) readonly coursesRepository: Repository<Course>,
    @InjectRepository(Mark) readonly marksRepository: Repository<Mark>,
  ) {}

  async assignMarkToStudent(createMarkDto: CreateMarkDto) {
    const { lectorId, studentId, courseId, ...markData } = createMarkDto;

    const lector = await this.lectorsRepository.findOne({
      where: {
        id: lectorId,
      },
    });
    if (!lector) {
      throw new HttpException(
        'Lector with this id NOT FOUND',
        HttpStatus.NOT_FOUND,
      );
    }
    const course = await this.coursesRepository.findOne({
      where: {
        id: courseId,
      },
    });
    if (!course) {
      throw new HttpException(
        'Course with this id NOT FOUND',
        HttpStatus.NOT_FOUND,
      );
    }
    const student = await this.studentsRepository.findOne({
      where: {
        id: studentId,
      },
    });
    if (!student) {
      throw new HttpException(
        'Student with this id NOT FOUND',
        HttpStatus.NOT_FOUND,
      );
    }

    const mark = this.marksRepository.create(markData);
    mark.lector = lector;
    mark.course = course;
    mark.student = student;

    return this.marksRepository.save(mark);
  }

  async getStudentMarks(studentId?: number) {
    const result = this.marksRepository
      .createQueryBuilder('mark')
      .leftJoinAndSelect('mark.course', 'course')
      .leftJoinAndSelect('mark.student', 'student');
    if (studentId) {
      result.where('mark.student_id = :studentId', { studentId });
    }

    const marks = await result
      .select([
        'course.name as "courseName"',
        'mark.mark as mark',
        'student.id as "studentId"',
      ])
      .orderBy('mark.createdAt')
      .getRawMany();

    return marks;
  }

  async getCourseMarks(courseId?: number) {
    const result = this.marksRepository.createQueryBuilder('mark');
    if (courseId) {
      result.where('mark.course_id = :courseId', { courseId });
    }

    const marks = await result
      .leftJoinAndSelect('mark.course', 'course')
      .leftJoinAndSelect('mark.lector', 'lector')
      .leftJoinAndSelect('mark.student', 'student')
      .select([
        'mark.mark as mark',
        'course.name as "courseName"',
        'student.name as "studentName"',
        'lector.name as "lectorName"',
        'course.id as "courseId"',
      ])
      .orderBy('mark.createdAt')
      .getRawMany();

    return marks;
  }
}
