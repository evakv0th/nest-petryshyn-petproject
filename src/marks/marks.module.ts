import { Module } from '@nestjs/common';
import { MarksService } from './marks.service';
import { MarksController } from './marks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from 'src/courses/entities/course.entity';
import { Lector } from 'src/lectors/entities/lector.entity';
import { Student } from 'src/students/entities/student.entity';
import { Mark } from './entities/mark.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Course, Lector, Student, Mark])],
  controllers: [MarksController],
  providers: [MarksService],
})
export class MarksModule {}
