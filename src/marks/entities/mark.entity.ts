import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'integer',
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Student, (student) => student.marks)
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Course, (course) => course.marks)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Lector, (lector) => lector.marks)
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
