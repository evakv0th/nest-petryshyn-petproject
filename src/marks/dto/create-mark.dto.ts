import { ApiProperty } from '@nestjs/swagger';
import { Min, Max, IsNotEmpty, IsNumber } from 'class-validator';
import { CourseResponse } from 'src/courses/dto/create-course.dto';
import { Course } from 'src/courses/entities/course.entity';
import { LectorResponse } from 'src/lectors/dto/create-lector.dto';
import { Lector } from 'src/lectors/entities/lector.entity';
import { StudentDtoResponse } from 'src/students/dto/create-student.dto';
import { Student } from 'src/students/entities/student.entity';

export class CreateMarkDto {
  @Min(0)
  @Max(100)
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  mark: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  studentId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  courseId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  lectorId: number;
}

export class MarkResponse {
  @ApiProperty()
  mark: number;
  @ApiProperty()
  course: CourseResponse;
  @ApiProperty()
  student: StudentDtoResponse;
  @ApiProperty()
  lector: LectorResponse;
  @ApiProperty()
  id: number;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
}

export class StudentMarksResponse {
  @ApiProperty()
  studentId: number;
  @ApiProperty()
  mark: number;
  @ApiProperty()
  courseName: string;
}

export class CourseMarksResponse {
  @ApiProperty()
  mark: number;
  @ApiProperty()
  courseId: number;
  @ApiProperty()
  lectorName: string;
  @ApiProperty()
  studentName: string;
  @ApiProperty()
  courseName: string;
}
