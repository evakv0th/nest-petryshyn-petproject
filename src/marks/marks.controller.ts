import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  HttpStatus,
} from '@nestjs/common';
import { MarksService } from './marks.service';
import {
  CourseMarksResponse,
  CreateMarkDto,
  MarkResponse,
  StudentMarksResponse,
} from './dto/create-mark.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';

@UseGuards(AuthGuard)
@Controller('marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @Post()
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Create  Mark' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'created Mark',
    type: MarkResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async assignMarkToStudent(@Body() createMarkDto: CreateMarkDto) {
    return this.marksService.assignMarkToStudent(createMarkDto);
  }

  @Get('/student')
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Get student Mark' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get student Mark',
    type: StudentMarksResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async getStudentMarks(@Query('studentId') studentId: number) {
    return this.marksService.getStudentMarks(studentId);
  }

  @Get('/course')
  @ApiBearerAuth('JWT-auth')
  @ApiOperation({ summary: 'Get course Mark' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get course Mark',
    type: CourseMarksResponse,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  async getCourseMarks(@Query('courseId') courseId: number) {
    return this.marksService.getCourseMarks(courseId);
  }
}
