import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  Max,
  Min,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Group } from 'src/groups/entities/group.entity';
import { GroupResponseDto } from 'src/groups/dto/create-group.dto';

export class CreateStudentDto {
  @IsEmail()
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  email: string;

  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  surname: string;

  @Min(10)
  @Max(123)
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  age: number;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  imagePath: string;
}

export class StudentDtoWithGroupName extends CreateStudentDto {
  @ApiProperty()
  groupName: string;
  @ApiProperty()
  id: number;
}

export class StudentDtoResponse extends CreateStudentDto {
  @ApiProperty()
  groupId: number;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  id: number;
}

export class StudentWithGroups extends StudentDtoResponse {
  @ApiProperty()
  group: GroupResponseDto[];
}
