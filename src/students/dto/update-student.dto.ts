import { PartialType } from '@nestjs/mapped-types';
import { CreateStudentDto } from './create-student.dto';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsOptional,
  IsString,
  Length,
  Min,
  Max,
  IsNumber,
  IsNotEmpty,
} from 'class-validator';

export class UpdateStudentDto extends PartialType(CreateStudentDto) {
  @IsEmail()
  @IsOptional()
  @IsString()
  @ApiProperty()
  email: string;

  @Length(2, 50)
  @IsOptional()
  @IsString()
  @ApiProperty()
  name: string;

  @Length(2, 50)
  @IsOptional()
  @IsString()
  @ApiProperty()
  surname: string;

  @Min(10)
  @Max(123)
  @IsOptional()
  @IsNumber()
  @ApiProperty()
  age: number;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  imagePath: string;
}
