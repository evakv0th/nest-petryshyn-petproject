import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { Repository } from 'typeorm';
import { Group } from 'src/groups/entities/group.entity';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student) private studentsRepository: Repository<Student>,
    @InjectRepository(Group) readonly groupsRepository: Repository<Group>,
  ) {}

  async create(createStudentDto: CreateStudentDto) {
    const student = await this.studentsRepository.findOne({
      where: [
        { email: createStudentDto.email },
        { name: createStudentDto.name, surname: createStudentDto.surname },
      ],
    });
    if (student) {
      throw new HttpException(
        'Student with this email or name+surname already exists',
        HttpStatus.CONFLICT,
      );
    }
    return this.studentsRepository.save(createStudentDto);
  }

  async findAll(name?: string) {
    const result = this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as "imagePath"',
      ])
      .orderBy('student.id')
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"');

    if (name) {
      result.where('student.name = :name', { name });
    }

    const students = await result.getRawMany();
    return students;
  }

  async findOne(id: number) {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as "imagePath"',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();
    if (!student) {
      throw new HttpException(
        'Student with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return student;
  }

  async update(id: number, updateStudentDto: UpdateStudentDto) {
    const result = await this.studentsRepository.update(id, updateStudentDto);
    if (!result.affected) {
      throw new HttpException(
        'Student with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }

  async remove(id: number) {
    const result = await this.studentsRepository.delete(id);
    if (!result.affected) {
      throw new HttpException(
        'Student with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }

  async addStudentToGroup(id: number, groupId: number) {
    const student = await this.studentsRepository.findOne({
      where: {
        id: id,
      },
    });
    if (!student) {
      throw new HttpException(
        'Student with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    const group = await this.groupsRepository.findOne({
      where: {
        id: groupId,
      },
    });

    if (!group) {
      throw new HttpException(
        'Group with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    student.group = group;
    await this.studentsRepository.save(student);
    return student;
  }
}
