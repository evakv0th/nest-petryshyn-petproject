import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group } from './entities/group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from 'src/students/entities/student.entity';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group) private groupsRepository: Repository<Group>,
    @InjectRepository(Student) private studentsRepository: Repository<Student>,
  ) {}
  async create(createGroupDto: CreateGroupDto) {
    const group = await this.groupsRepository.findOne({
      where: {
        name: createGroupDto.name,
      },
    });
    if (group) {
      throw new HttpException(
        'Group with this name already exists',
        HttpStatus.NOT_FOUND,
      );
    }
    return this.groupsRepository.save(createGroupDto);
  }

  async findAll() {
    const groups = await this.groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .orderBy('group.id')
      .select([
        'group.id',
        'group.name',
        'group.createdAt',
        'group.updatedAt',
        'student.name',
        'student.surname',
        'student.age',
        'student.email',
      ])
      .getMany();
    return groups;
  }

  async findOne(id: number) {
    const group = await this.groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .where('group.id = :id', { id })
      .getOne();
    if (!group) {
      throw new HttpException(
        'Group with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return group;
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    const result = await this.groupsRepository.update(id, updateGroupDto);
    if (!result.affected) {
      throw new HttpException(
        'Group with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }

  async remove(id: number) {
    const students = await this.studentsRepository.find({
      where: {
        groupId: id,
      },
    });
    if (students) {
      const updatePromises = students.map(async (student) => {
        student.groupId = null;
        await this.studentsRepository.save(student);
      });

      await Promise.all(updatePromises);
    }
    console.log(students);
    const result = await this.groupsRepository.delete(id);
    if (!result.affected) {
      throw new HttpException(
        'Group with this id not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return result;
  }
}
