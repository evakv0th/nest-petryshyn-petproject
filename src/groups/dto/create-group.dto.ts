import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { StudentDtoResponse } from 'src/students/dto/create-student.dto';

export class CreateGroupDto {
  @Length(2, 50)
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;
}

export class GroupResponseDto extends CreateGroupDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  createdAt: Date;
}

export class GroupWithStudents extends GroupResponseDto {
  @ApiProperty({ type: () => StudentDtoResponse, isArray: true })
  students: StudentDtoResponse;
}
