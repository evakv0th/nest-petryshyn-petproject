export interface ResetTokenInterface {
  username: string;
  token: string;
}
