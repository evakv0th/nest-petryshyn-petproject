import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRelationshipStudentsAndMarks1692480122260
  implements MigrationInterface
{
  name = 'AddRelationshipStudentsAndMarks1692480122260';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "marks" ADD "student_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`,
    );
    await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "student_id"`);
  }
}
