import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateStudentsEntity1692478441573 implements MigrationInterface {
  name = 'CreateStudentsEntity1692478441573';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "students" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "surname" character varying NOT NULL, "email" character varying NOT NULL, "age" numeric, "imagePath" character varying, CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b5e856b621a7b64cdf48059067" ON "students" ("name") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX "public"."IDX_b5e856b621a7b64cdf48059067"`,
    );
    await queryRunner.query(`DROP TABLE "students"`);
  }
}
