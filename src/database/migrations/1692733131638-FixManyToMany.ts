import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixManyToMany1692733131638 implements MigrationInterface {
  name = 'FixManyToMany1692733131638';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "lectorCourses" ("coursesId" integer NOT NULL, "lectorsId" integer NOT NULL, CONSTRAINT "PK_28a66e9110c2114f01022116d7a" PRIMARY KEY ("coursesId", "lectorsId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e93c0831eb277a0baec0267149" ON "lectorCourses" ("coursesId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_2e668ddd0609b419ff94f982e4" ON "lectorCourses" ("lectorsId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "lectorCourses" ADD CONSTRAINT "FK_e93c0831eb277a0baec02671494" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectorCourses" ADD CONSTRAINT "FK_2e668ddd0609b419ff94f982e40" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectorCourses" DROP CONSTRAINT "FK_2e668ddd0609b419ff94f982e40"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectorCourses" DROP CONSTRAINT "FK_e93c0831eb277a0baec02671494"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_2e668ddd0609b419ff94f982e4"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_e93c0831eb277a0baec0267149"`,
    );
    await queryRunner.query(`DROP TABLE "lectorCourses"`);
  }
}
