import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRelationshipCoursesAndMarks1692480302679
  implements MigrationInterface
{
  name = 'AddRelationshipCoursesAndMarks1692480302679';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "marks" ADD "course_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`,
    );
    await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "course_id"`);
  }
}
